import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {PostsService} from '../services/posts.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-post-component',
  templateUrl: './new-post-component.component.html',
  styleUrls: ['./new-post-component.component.scss']
})
export class NewPostComponentComponent implements OnInit {

  constructor(private postsService: PostsService, private router: Router) { }
  ngOnInit() {
  }
  onSubmit(form: NgForm) {
    const titre = form.value['title'];
    const contenu = form.value['content'];
    const loveIts = 0;
    const created = new Date();
    this.postsService.newPost(titre, contenu, loveIts, created);
    this.router.navigate(['/posts']);
  }

}
