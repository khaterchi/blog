import {Component, Input, OnInit} from '@angular/core';
import {PostsService} from '../services/posts.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() postTitle: string;
  @Input() postContent: string;
  @Input() postLikes: number;
  @Input() postCreate: Date;
  @Input() index: number;
  constructor(private postsService: PostsService, private router: Router) { }

  ngOnInit() {
  }
  onAnnulLike() {
    if (confirm('Etes-vous sûr de vouloir annuler les Likes ?')) {
      this.postLikes = this.postsService.setAnnulLike(this.index);
    } else {
      return null;
    }
  }
  onSetLikes() {
    this.postLikes = this.postsService.setLikes(this.index);
  }
  onSetDislikes() {
    this.postLikes = this.postsService.setDislikes(this.index);
  }
  onRemovePost() {
    if (confirm('Etes-vous sûr de vouloir supprimer ce post ?')) {
      this.postsService.removePost(this.index);
    } else {
      return null;
    }
  }
}
