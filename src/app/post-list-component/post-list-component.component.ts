import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import { PostsService } from '../services/posts.service';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.scss']
})
export class PostListComponentComponent implements OnInit, OnDestroy {

  posts: any[];
  postsSubscription: Subscription;
  constructor(private postsServices: PostsService, private router: Router) {}
  ngOnInit(): void {
    this.postsSubscription = this.postsServices.postsSubject.subscribe(
      (value: any[]) => {
        this.posts = value;
      }
    );
    this.postsServices.emitPostsSubject();
  }
  ngOnDestroy(): void {
    this.postsSubscription.unsubscribe();
  }
}
