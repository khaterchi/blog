import {Subject} from 'rxjs';

export class PostsService {
  postsSubject = new Subject<any[]>();
  private posts = [
    {
      title: 'Mon premier Post',
      content: 'In this blog post, we are going to look at a new API in Angular CLI,' +
        'which allows you to add CLI features and augment existing ones.' +
        'We ll discuss how to interact with this API and what are the extension points which' +
        'allow you to add additional features to the CLI.',
      loveIts: 1,
      created: new Date()
    },
    {
      title: 'Mon deuxième Post',
      content: 'A common challenge developers have to tackle when building applications consuming' +
        'large data sets, is how to create a maintainable and scalable user experience for editing.' +
        'They may face hundreds of thousands and even millions of records on which they execute CRUD ' +
        'operations that need to be sent to a server and saved to a database.\n' +
        'They want to make sure no redundant calls to the server are executed.',
      loveIts: 0,
      created: new Date()
    },
    {
      title: 'Encore un Post',
      content: '“When will Ivy be ready?” is a question we get asked every week.' +
        'Here s an update on how things are going with version 8.0, and our plans for releasing and finalizing Ivy.' +
        'We are planning on having Ivy as an opt-in preview as part of the version 8.0 release in Q2 of this year.',
      loveIts: -1,
      created: new Date()
    }
  ];
  constructor() {}
  emitPostsSubject() {
  this.postsSubject.next(this.posts.slice());
  }

  onChangeDate() {
    for (let post of this.posts) {
      post.created = new Date();
      this.emitPostsSubject();
    }
  }
  setAnnulLike(index: number) {
    this.posts[index].loveIts = 0;
    this.emitPostsSubject();
    return this.posts[index].loveIts;
  }
  setLikes(index: number) {
    this.posts[index].loveIts++;
    this.emitPostsSubject();
    return this.posts[index].loveIts;
  }
  setDislikes(index: number) {
    this.posts[index].loveIts--;
    this.emitPostsSubject();
    return this.posts[index].loveIts;
  }
  newPost(title: string, content: string, loveIts: number, created: Date) {
    const postObject = {
      title: '',
      content: '',
      loveIts: 0,
      created: new Date()
    };
    postObject.title = title;
    postObject.content = content;
    postObject.loveIts = loveIts;
    postObject.created = created;

    this.posts.push(postObject);
    this.emitPostsSubject();
  }
  removePost(index: number) {
    this.posts.splice(index, 1);
    this.emitPostsSubject();
  }
}
