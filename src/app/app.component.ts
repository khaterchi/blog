import {Component, OnDestroy, OnInit} from '@angular/core';
import {PostsService} from './services/posts.service';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'blog';
  ngOnInit(): void {
  }
}
